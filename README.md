# Utilisation de Git

Comment se servir de Git ?

**Paramétrer son Git**
```git
git config --global user.name "<username>"
git config --global user.email "<email>"
```

**Créer un nouveau répertoire**
```git
git clone https://gitlab.com/<username>/<repository>.git
```
```
cd <repository>
touch README.md
```

**Pousser un élément dans votre répertoire**
```
git add <file>
git commit -m "<commentaire>"
git push
```
**Tirer dans son répertoire local de nouveaux fichiers ajoutés dans votre répertoire sur GitLab**
```
cd <repertoire>
git pull origin master
```